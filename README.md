# brettops-cluster-digitalocean

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/brettops/terraform/deployments/brettops-cluster-digitalocean?branch=main)](https://gitlab.com/brettops/terraform/deployments/brettops-cluster-digitalocean/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->
