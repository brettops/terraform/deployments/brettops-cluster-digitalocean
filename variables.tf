variable "environment" {
  type = string
}

variable "vpc_ip_range" {
  type = string
}
