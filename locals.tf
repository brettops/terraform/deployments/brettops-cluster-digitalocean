locals {
  deployment  = "brettops"
  environment = var.environment

  name = "${local.environment}-${local.deployment}"

  region = "sfo3"

  vpc_ip_range = var.vpc_ip_range

  tags = [
    "deployment:${local.deployment}",
    "environment:${local.environment}",
  ]
}
