module "root" {
  source = "../.."

  environment = "staging"

  vpc_ip_range = "10.11.0.0/16"
}
