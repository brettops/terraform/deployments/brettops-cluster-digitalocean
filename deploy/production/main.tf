module "root" {
  source = "../.."

  environment = "production"

  vpc_ip_range = "10.10.0.0/16"
}
