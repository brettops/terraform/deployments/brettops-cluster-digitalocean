resource "digitalocean_vpc" "cluster" {
  name     = local.name
  region   = local.region
  ip_range = local.vpc_ip_range
}

resource "digitalocean_kubernetes_cluster" "cluster" {
  auto_upgrade  = true
  name          = local.name
  region        = local.region
  surge_upgrade = true
  version       = data.digitalocean_kubernetes_versions.cluster.latest_version
  vpc_uuid      = digitalocean_vpc.cluster.id

  node_pool {
    auto_scale = true
    name       = "${local.name}-default"
    max_nodes  = 5
    min_nodes  = 1
    size       = "s-2vcpu-2gb"
    tags       = local.tags
  }
}
